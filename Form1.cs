﻿
namespace CalculatorT
{
    public partial class Calculator : Form
    {
        double result = 0;
        
        string operation = "";
        bool enterValue = false;
        bool isEqual = false;
        public Calculator()
        {
            InitializeComponent();
        }

        private void NumbersOnly(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (b == btnPlusMinus)
            {
                if (txtDisplay.Text == "0") return;

                if (txtDisplay.Text.Contains("-"))
                {
                    txtDisplay.Text = txtDisplay.Text.Substring(1);
                }
                else
                {
                    txtDisplay.Text = $"-{txtDisplay.Text}";
                }
                return;
            }


            if ((txtDisplay.Text == "0") || (enterValue))
            {
                txtDisplay.Text = "";
                enterValue = false;

                if (isEqual)
                {
                    txtDisplayMini.Text = "";
                    isEqual = false;
                }
            }
            if (b == btnDot)
            {
                if (!txtDisplay.Text.Contains(","))
                    txtDisplay.Text = txtDisplay.Text + b.Text;

            }
            else
            {
                txtDisplay.Text = txtDisplay.Text + b.Text;
            }
            
        }

        private void Operations(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            operation = b.Text;

            result = Double.Parse(txtDisplay.Text);

            enterValue = true;
            if (operation == "√x")
            {
                txtDisplayMini.Text = $"√{result}";
            }
            else if (operation == "x²")
            {
                txtDisplayMini.Text = $"{result}²";
            }
            else if(operation == "%") 
            {
                txtDisplayMini.Text = $"{result}%";
            }
            else
            {
                txtDisplayMini.Text = result.ToString() + " " + operation;
            }



        }

        private void Operations2(object sender, EventArgs e)
        {
            if (sender == btnEqual)
            {
                switch (operation)
                {
                    case "+":
                        txtDisplayMini.Text = $"{result} + {txtDisplay.Text} = ";
                        txtDisplay.Text = (result + double.Parse(txtDisplay.Text)).ToString();

                        break;
                    case "-":
                        txtDisplayMini.Text = $"{result} - {txtDisplay.Text} = ";
                        txtDisplay.Text = (result - double.Parse(txtDisplay.Text)).ToString();

                        break;
                    case "x":
                        txtDisplayMini.Text = $"{result} x {txtDisplay.Text} = ";
                        txtDisplay.Text = (result * double.Parse(txtDisplay.Text)).ToString();
                        break;
                    case "÷":
                        txtDisplayMini.Text = $"{result} / {txtDisplay.Text} = ";
                        txtDisplay.Text = (result / double.Parse(txtDisplay.Text)).ToString();
                        break;
                    case "x²":
                        txtDisplayMini.Text = $"{txtDisplay.Text}² = ";
                        txtDisplay.Text = (Math.Pow(double.Parse(txtDisplay.Text), 2).ToString());
                        break;
                    case "√x":
                        txtDisplayMini.Text = $"√({txtDisplay.Text}) = ";
                        txtDisplay.Text = (Math.Sqrt(double.Parse(txtDisplay.Text)).ToString());
                        break;
                    case "%":
                        txtDisplayMini.Text = $"{txtDisplay.Text}% = ";
                        txtDisplay.Text = ((double.Parse(txtDisplay.Text)/100).ToString());
                        break;
                    default:
                        break;
                }
                enterValue = true;
                isEqual = true;
            }

            if(sender == btnC) 
            {
                result = 0;
                txtDisplay.Text = "0";
                txtDisplayMini.Text = "";
            }
            if (sender == btnCE)
            {
                txtDisplay.Text = "0";
            }
            if(sender == btnUndo && !string.IsNullOrEmpty(txtDisplay.Text)) 
            {
                txtDisplay.Text = txtDisplay.Text.Substring(0, txtDisplay.Text.Length - 1);
            }
        }
    }
}